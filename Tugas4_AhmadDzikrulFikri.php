<!DOCTYPE html>

<!-- Ahmad Dzikrul Fikri -->

<html>
<head>
	<title>Tugas 4 - Membuat Program Sederhana</title>
</head>
<body>

<form method="post"> <!-- pembuatan form penerima jumlah bintang -->
	Jumlah Bintang= <input type="text" name="jumlah"/> <!-- input jumlah bintang -->
	<br />
	<input type="submit" value="kirim"> <!-- tombol kirim form -->
</form>
<?php
if (isset($_POST['jumlah'])) { // conditional if yang digunakan apabila form telah diisi. jika belum diisi blok script tidak akan dijalankan
	$jumlahBintang = $_POST['jumlah']; // pendeklarasian vaiabel jumlah
	for ($i=0; $i < $jumlahBintang; $i++) { // iterasi sejumlah bintang yang diminta
		for ($j=0; $j <= $i; $j++) { // iterasi sejumlah variabel i yang nantinya akan digunakan untuk mencetak bintang pada baris ke-i sebanyak i
		 	echo "*";
		 } 
		echo "<br>";
	}
}

 ?>
</body>
</html>