import math
import numpy as np
def plouff(rho,u_ijk,x_l,y_m,z_n,r_ijk):
    return 1*(6.67*10**-11)*rho*u_ijk*(-x_l*math.log(y_m+r_ijk)-y_m*math.log(x_l+r_ijk)+z_n*np.arctan((x_l*y_m)/z_n*r_ijk))

def sorokin(rho,u_ijk,x_l,y_m,z_n,r_ijk):
    return -1*(6.67*10**-11)*rho*u_ijk*(x_l*math.log(y_m+r_ijk)+y_m*math.log(x_l+r_ijk)+z_n*np.arctan((z_n*r_ijk)/(x_l*y_m)))

def okabe(rho,u_ijk,x_l,y_m,z_n,r_ijk):
    return -1*(6.67*10**-11)*rho*u_ijk*(x_l*math.log(y_m+r_ijk)+y_m*math.log(x_l+r_ijk)+2*z_n*np.arctan((x_l+y_m+r_ijk)/(z_n)))

def nagy(rho,u_ijk,x_l,y_m,z_n,r_ijk):
    return -1*(6.67*10**-11)*rho*u_ijk*(x_l*math.log(y_m+r_ijk)+y_m*math.log(x_l+r_ijk)-z_n*np.arcsin((y_m**2+z_n**2+y_m*r_ijk)/((y_m+r_ijk)*math.sqrt(y_m**2+z_n**2))))