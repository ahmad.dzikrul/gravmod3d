from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('model3d/', views.model3d, name='model3d'),
    path('model3d/<path:list_data>/', views.model3dplus, name='model3dplus'),
    path('model3dnew/<path:list_data>/', views.model3dnew, name='model3dnew'),
    path('register/', views.register, name='register'),
    path('custom/', views.custom, name='custom'),
    path('custom/<path:list_data>/', views.custom_plus, name='custom_plus'),
    path('process/<path:list_data>/', views.process, name='process')
]
