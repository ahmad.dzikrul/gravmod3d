from django.shortcuts import redirect,render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
import matplotlib.pyplot as plt
import numpy as np
import io, base64
import math

def home(request):
    return render(request, 'main/home.html')

def custom(request):
    return render(request, 'main/custom.html')

def custom_plus(request,list_data):
    if request.user.is_authenticated:
        cek_user = "logged in"
    else:
        cek_user = "not logged in"
    response = {
        'cek_user' : cek_user,
        'list_data' : list_data
    }
    return render(request, 'main/custom.html',response)

def process(request,list_data):
    if request.user.is_authenticated:
        cek_user = "logged in"
    else:
        cek_user = "not logged in"
    response = {
        'cek_user' : cek_user,
        'list_data' : list_data
    }
    return render(request, 'main/process.html',response)

def model3d(request):
    if request.user.is_authenticated:
        cek_user = "logged in"
    else:
        cek_user = "not logged in"

    response = {
        'cek_user' : cek_user
    }
    return render(request, 'main/model.html',response)

class box:
  def __init__(self, x, y, z,d):
    self.x = x
    self.y = y
    self.z = z
    self.d = d

def model3dplus(request,list_data):
    if request.user.is_authenticated:
        cek_user = "logged in"
    else:
        cek_user = "not logged in"
    response = {
        'cek_user' : cek_user,
        'list_data' : list_data
    }
    return render(request, 'main/model.html',response)

def model3dnew(request,list_data):
    if request.user.is_authenticated:
        cek_user = "logged in"
    else:
        cek_user = "not logged in"
    data_olah = decoder(list_data)
    boxs = get_data(data_olah[:-2])
    cba_image = cba(boxs,data_olah[-2],data_olah[-1])
    response = {
        'cek_user' : cek_user,
        'list_data' : list_data,
        'cba_image' : cba_image
    }
    return render(request, 'main/result.html',response)

def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()
    
    context = {"form":form}
    return render(request, 'registration/register.html', context)

def auth_check(request):
    if request.user.is_authenticated:
        response = "logged in"
    else:
        response = "not logged in"
    return response

def cba(boxs,deep,metode):
    x = []
    y = []
    z = []
    d = []
    for i in boxs[0] :
        x.append(i.x)
        y.append(i.y)
        z.append(i.z)
        d.append(i.d)
    
    x = np.array(x)
    y = np.array(y)
    z = np.array(z)
    ### Melakukan Gridding Koordinat ###
    x = np.array([x,x+50])
    y = np.array([y,y+50])
    z = np.array([z,z+50])
    d = np.array(d)
    jumlah_box = len(boxs[0])

    x0 = x[0]
    xt = x[1]
    y0 = y[0]
    yt = y[1]
    z0 = z[0]
    zt = z[1]

    if len(boxs) == 2:
        for i in boxs[1]:
            x0 = np.append(x0,i[0])
            xt = np.append(xt,i[1])
            y0 = np.append(y0,i[4])
            yt = np.append(yt,i[5])
            z0 = np.append(z0,i[2])
            zt = np.append(zt,i[3]) 
            d = np.append(d,i[6])

        x = np.array([x0,xt])
        y = np.array([y0,yt])
        z = np.array([z0,zt])
        jumlah_box = len(boxs[0])+len(boxs[1])

    z*=-1
    y = np.array([deep - y[0], deep - y[1]])*-1
    ngrid = 50 # jumlah station pengukuran


    x_grid = np.linspace(-1197, 1199, ngrid) #grid koordinat
    z_grid = np.linspace(-1198, 1198, ngrid) #grid koordinat

    X, Z = np.meshgrid(x_grid, z_grid)

    g_station = []
    for i in range(len(x_grid)):
        
        baris = []
        for j in range(len(z_grid)):
            g_tot = 0
            for k in range(jumlah_box): # looping terhadap jumlah box
                for l in range(2):
                    for m in range(2):
                        for n in range(2):
                            u_ijk = (-1)**(l+1)*(-1)**(m+1)*(-1)**(n+1)
                            r_ijk = ((x_grid[j]-x[l][k])**2 + (z_grid[i]-z[m][k])**2 + y[n][k]**2)
                            x_l = x_grid[j] - x[l][k]
                            y_n = -y[1-n][k] #pada data dari web, koordinat y menunjukkan kedalaman
                            z_m = z_grid[i] - z[m][k]
                            if metode == "sorokin":
                                g_tot += sorokin(d[k],u_ijk,x_l,z_m,y_n,r_ijk)
                            elif metode == "plouff":
                                g_tot += plouff(d[k],u_ijk,x_l,z_m,y_n,r_ijk)
                            elif metode == "okabe":
                                g_tot += okabe(d[k],u_ijk,x_l,z_m,y_n,r_ijk)
                            else:
                                g_tot += nagy(d[k],u_ijk,x_l,z_m,y_n,r_ijk)
            g_tot *= 100000 #konversi satuan ke miligal
            baris.append(g_tot)
        g_station.append(baris)

    plt.contourf(X,Z, g_station, cmap='jet', levels=300)
    plt.colorbar()
    plt.xlabel('Easting (m)', fontsize='16', fontweight='bold')
    plt.ylabel('Northing (m)', fontsize='16', fontweight='bold')
    plt.title('Peta Respon gravitasi (mgal)', fontsize='16', fontweight='bold')
    flike = io.BytesIO()
    plt.savefig(flike)
    b64 = base64.b64encode(flike.getvalue()).decode()
    plt.close()
    return b64


def decoder(string_data):
    alf_code = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F"]
    bin_code = ["00000","00001","00010","00011","00100","00101","00110","00111","01000","01001","01010","01011","01100","01101","01110","01111","10000","10001","10010","10011","10100","10101","10110","10111","11000","11001","11010","11011","11100","11101","11110","11111"]
    output = []
    data_box = []
    data_densities = []
    custom_box = []
    data_box_custom = []
    string_data = string_data.split("[")
    deep = int(string_data[-2])
    metode = string_data[-1]
    for i in string_data[0][2:].split(";"):
        if i in alf_code:
            i = bin_code[alf_code.index(i)]
        data_box.append(i)
    output.append(data_box)
    for i in string_data[1].split(","):
        data_densities.append(float(i))
    output.append(data_densities)
    if len(string_data) == 5:
        data_custom = string_data[2].split("]")
        for i in data_custom[:-1]:
            split_i = i.split(":")
            for j in range(7):
                if j<6:
                    custom_box.append(int(split_i[j]))
                else :
                    custom_box.append(float(split_i[j]))
            data_box_custom.append(custom_box)
            custom_box = []
        return [output,data_box_custom,deep,metode]
    else:
        return [output,deep,metode]

def get_data(list_data):
    current = {'x': 0, 'y': 0, 'z': 0, 'c': 0}
    if len(list_data) == 2:
        data_box = list_data[0][0]
        data_densities = list_data[0][1]
        data_box_custom = list_data[1]
    else:
        data_box = list_data[0][0]
        data_densities = list_data[0][1]
    i = 0
    l = len(data_box)
    boxs = []

    while i<l-1 : # di -1 soalnya hasil split data di decoder ada string kosong diakhir
        code = data_box[i]
        i+=1

        if code[1] == "1":
            current["x"] += int(data_box[i])  # dapet posisi x (buat dapet koordinat harus dikaliin 50 atau grid lapangan biar tau)
            i+=1
        if code[2] == "1":
            current["y"] += int(data_box[i]) # dapet posisi y (buat dapet koordinat harus dikaliin 50 atau grid lapangan biar tau)
            i+=1
        if code[3] == "1":
            current["z"] += int(data_box[i]) # dapet posisi z (buat dapet koordinat harus dikaliin 50 atau grid lapangan biar tau)
            i+=1
        if code[4] == "1":
            current["c"] += int(data_box[i]) # dapet index c yang mana menunjukkan juga index d
            i+=1
        if code[0] == "1":
            boxs.append(box(current["x"]*50,current["y"]*50,current["z"]*50,data_densities[current["c"]]))
            
    
    if len(list_data) == 2:
        return [boxs,data_box_custom]
    else:
        return [boxs]

def plouff(rho,u_ijk,x_l,y_m,z_n,r_ijk):
    return -1*(6.67*10**-11)*rho*u_ijk*(-x_l*math.log(y_m+r_ijk)-y_m*math.log(x_l+r_ijk)+z_n*np.arctan((x_l*y_m)/(z_n*r_ijk)))

def sorokin(rho,u_ijk,x_l,y_m,z_n,r_ijk):
    return -1*(6.67*10**-11)*rho*u_ijk*(x_l*math.log(y_m+r_ijk)+y_m*math.log(x_l+r_ijk)+z_n*np.arctan((z_n*r_ijk)/(x_l*y_m)))

def okabe(rho,u_ijk,x_l,y_m,z_n,r_ijk):
    return -1*(6.67*10**-11)*rho*u_ijk*(x_l*math.log(y_m+r_ijk)+y_m*math.log(x_l+r_ijk)+2*z_n*np.arctan((x_l+y_m+r_ijk)/(z_n)))

def nagy(rho,u_ijk,x_l,y_m,z_n,r_ijk):
    return -1*(6.67*10**-11)*rho*u_ijk*(x_l*math.log(y_m+r_ijk)+y_m*math.log(x_l+r_ijk)-z_n*np.arcsin((y_m**2+z_n**2+y_m*r_ijk)/((y_m+r_ijk)*math.sqrt(y_m**2+z_n**2))))